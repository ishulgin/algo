#include <iostream>
#include <map>
#include <string>

using namespace std;

struct directory {
    map<string, directory> children;

    void process_child(const string &path) {
        char name_size = path.find('\\');

        if (name_size != -1)
            children[path.substr(0, name_size)].process_child(path.substr(name_size + 1, path.length()));
        else
            children[path.substr(0, name_size)];
    }

    void print(unsigned char depth = 0) {
        for (auto &child : children) {
            printf("%*s%s\n", depth, "", child.first.c_str());

            child.second.print(depth + 1);
        }
    }
};


int main() {
    char current_str[81];
    directory root;

    unsigned short n;
    scanf("%hu", &n);

    while (n--) {
        scanf("%s", current_str);
        root.process_child(current_str);
    }

    root.print();

    return 0;
}
