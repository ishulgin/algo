#define max(a, b) (((a) > (b)) ? (a) : (b))

#include <stdio.h>

int main(void) {
	unsigned short n;
	int current_value, current_sum = 0, max_sum = 0;
	
	scanf("%hu", &n);

	while (n--) {
		scanf("%d", &current_value);
		if (current_value + current_sum > 0) {
			current_sum += current_value;
			max_sum = max(current_sum, max_sum);
		}
		else {
			current_sum = 0;
		}
	}

	printf("%d", max_sum);

	return 0;
}