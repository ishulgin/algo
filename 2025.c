#include <stdio.h>

#define GROUP_SIZE (n / k)
#define REMAINING (n % k)
#define SMALL_GROUPS_FIGHTERS_COUNT (n - (GROUP_SIZE + 1) * REMAINING)
#define BIG_GROUPS_FIGHTERS_COUNT (REMAINING * (GROUP_SIZE + 1))
#define RESULT (((n - GROUP_SIZE) * SMALL_GROUPS_FIGHTERS_COUNT + (n - GROUP_SIZE - 1) * BIG_GROUPS_FIGHTERS_COUNT) / 2)

int main() {
    unsigned char test_count;
    // k - команды, n - челики
    unsigned short n, k;

    scanf("%hhu", &test_count);

    while (test_count--) {
        scanf("%hu %hu", &n, &k);
        printf("%u\n", RESULT);
    }

    return 0;
}