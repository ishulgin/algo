#include <cstdio>
#include <algorithm>

struct point {
    int x, y;
    unsigned int id;
} points[10000];

bool cmp(struct point first, struct point second) {
    return (long long) (second.x - first.x) * (points[0].y - first.y) -
           (long long) (second.y - first.y) * (points[0].x - first.x) < 0;
}

int main() {
    unsigned int n;

    scanf("%u", &n);

    for (unsigned int i = 0; i < n; i++) {
        scanf("%d %d", &points[i].x, &points[i].y);
        points[i].id = i;
    }

    std::sort(&points[1], &points[n], cmp);

    printf("1 %d\n", points[n / 2].id + 1);
}
