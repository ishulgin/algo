#define ROW_TOP_LEFT 1, 0
#define ROW_BOTTOM_RIGHT m, n + 1

#define COL_TOP_LEFT 0, 1
#define COL_BOTTOM_RIGHT m + 1, n

#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
    unsigned int m, n, k;
    unsigned int x, y;
    unsigned int ans = 0;

    int y_diff, x_diff;

    vector<pair<int, int>> points;
    vector<pair<int, int>> single_points;

    scanf("%u %u %u", &m, &n, &k);

    points.reserve(k);

    for (int i = 0; i < k; i++) {
        scanf("%u %u", &x, &y);

        points.emplace_back(x, y);
    }

    points.emplace_back(ROW_TOP_LEFT);
    points.emplace_back(ROW_BOTTOM_RIGHT);

    sort(points.begin(), points.end());

    for (unsigned int i = 1; i < points.size(); i++) {
        y_diff = (unsigned int) points[i].first - points[i - 1].first;

        if (y_diff == 0) {
            x_diff = (unsigned int) points[i].second - points[i - 1].second;

            if (x_diff == 2) {
                single_points.emplace_back(points[i].first, points[i].second - 1);
            } else if (x_diff > 2) {
                ans++;
            }
        } else {
            if (points[i - 1].second == n - 1) {
                single_points.emplace_back(points[i - 1].first, n);
            } else if (points[i - 1].second < n - 1) {
                ans++;
            }

            if (n > 1) {
                ans += y_diff - 1;
            } else if (y_diff == 2) {
                single_points.emplace_back(points[i].first - 1, 1);
            }

            if (points[i].second == 2) {
                single_points.emplace_back(points[i].first, 1);
            } else if (points[i].second > 2) {
                ans++;
            }
        }
    }

    points.emplace_back(COL_TOP_LEFT);
    points.emplace_back(COL_BOTTOM_RIGHT);

    sort(points.begin(), points.end(), [](auto a, auto b) {
        return a.second == b.second ? a.first < b.first : a.second < b.second;
    });

    for (unsigned int i = 2; i < points.size() - 1; i++) {
        y_diff = points[i].second - points[i - 1].second;

        if (y_diff == 0) {
            x_diff = points[i].first - points[i - 1].first;

            if (x_diff == 2) {
                single_points.emplace_back(points[i].first - 1, points[i].second);
            } else if (x_diff > 2) {
                ans++;
            }
        } else {
            if (points[i - 1].first == m - 1) {
                single_points.emplace_back(m, points[i - 1].second);
            } else if (points[i - 1].first < m - 1) {
                ans++;
            }

            if (m > 1) {
                ans += y_diff - 1;
            } else if (y_diff == 2) {
                single_points.emplace_back(1, points[i].second - 1);
            }

            if (points[i].first == 2) {
                single_points.emplace_back(1, points[i].second);
            } else if (points[i].first > 2) {
                ans++;
            }
        }
    }

    sort(single_points.begin(), single_points.end());

    unsigned int dif = (unsigned int) (single_points.end() - unique(single_points.begin(), single_points.end()));

    printf("%d", ans + dif);

    return 0;
}