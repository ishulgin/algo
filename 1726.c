#include <stdio.h>
#include <stdlib.h>

int cmp(const void *first, const void *second) {
    return *(unsigned int *) first - *(unsigned int *) second;
}

int main() {
    unsigned int n;
    long long result = 0;

    scanf("%u", &n);

    unsigned int x[n], y[n];

    for (unsigned int i = 0; i < n; i++) {
        scanf("%u %u", &x[i], &y[i]);
    }

    qsort(x, n, sizeof(unsigned int), cmp);
    qsort(y, n, sizeof(unsigned int), cmp);

    for (unsigned int i = 1; i < n; i++) {
        result += (long long) (x[i] - x[i - 1] + y[i] - y[i - 1]) * i * (n - i);
    }

    result *= 2;
    result /= (long long) n * (n - 1);

    printf("%lld\n", result);

    return 0;
}
