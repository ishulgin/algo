#include <stdio.h>

#define push(stack_pointer, value) (*((stack_pointer)++) = (value))
#define pop(stack_pointer) (* --(stack_pointer))

int main() {
    unsigned char flag = 1;

    unsigned int current_order = 0;
    unsigned int stack_order = 0;

    unsigned int stack_top_value = 0;

    unsigned int n;

    scanf("%u", &n);

    unsigned int balls_order[n];
    unsigned int stack_base[n];

    unsigned int *stack_pointer = stack_base;

    for (unsigned int i = 0; i < n; i++) {
        scanf("%u", &balls_order[i]);
    }

    while (current_order < n && flag) {
        if (stack_pointer != stack_base && stack_top_value == balls_order[current_order]) {
            pop(stack_pointer);
            stack_top_value = pop(stack_pointer);
            push(stack_pointer, stack_top_value);

            current_order++;
        } else if (stack_order < n) {
            stack_order++;
            stack_top_value = stack_order;
            push(stack_pointer, stack_order);
        } else {
            flag = 0;
        }
    }

    puts(flag ? "Not a proof" : "Cheater");

    return 0;
}
