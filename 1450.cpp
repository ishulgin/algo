#define NOT_CHECKED (-1)
#define IMPOSSIBLE (-2)

#include <cstdio>
#include <algorithm>
#include <vector>
#include <climits>

using namespace std;

struct node {
    vector<pair<unsigned int, unsigned int>> outgoing_edges;
} nodes[501];

int dist[501];

unsigned int s, f;

int solve(unsigned int node_number) {
    int &result = dist[node_number];
    int temp_dist;
    int max_dist = INT_MIN;

    if (node_number != f && result == NOT_CHECKED) {
        for (auto &edge : nodes[node_number].outgoing_edges) {
            temp_dist = solve(edge.first);

            if (temp_dist != IMPOSSIBLE) {
                temp_dist += edge.second;

                max_dist = max(max_dist, temp_dist);
            }
        }

        result = max(IMPOSSIBLE, max_dist);
    } else if (node_number == f) {
        result = 0;
    }

    return result;
}

int main() {
    unsigned int n, m;
    unsigned int a, b;
    unsigned int c;

    int result;

    fill_n(dist, 501, NOT_CHECKED);

    scanf("%u %u", &n, &m);

    for (unsigned int i = 0; i < m; i++) {
        scanf("%u %u %u", &a, &b, &c);

        nodes[a].outgoing_edges.emplace_back(make_pair(b, c));
    }

    scanf("%u %u", &s, &f);

    result = solve(s);

    if (result == IMPOSSIBLE) {
        puts("No solution");
    } else {
        printf("%d", result);
    }

    return 0;
}