#define are_connected(a, b) ((a) != (b) & abs((a) - (b)) != 2)
#define same_side_diagonal(a) (((a) < 3 ? 0 : 4) + ((a) + 2) % 4)

#include <stdio.h>
#include <stdlib.h>

unsigned char vertices[8];

unsigned short pointer = 0;
char result_string[1000 * 3];

unsigned char max_element_pos_crossed(unsigned char first, unsigned char last, unsigned char crossed) {
    unsigned char max_pos = (first == crossed) ? first + 1 : first;
    for (unsigned char i = first; i < last; i++) {
        if (vertices[max_pos] < vertices[i] && i != crossed)
            max_pos = i;
    }
    return max_pos;
}

unsigned char max_element_pos(unsigned char first, unsigned char last) {
    return max_element_pos_crossed(first, last, last);
}

void add_edges(unsigned char first, unsigned char second, unsigned char n) {
    while (n--) {
        result_string[pointer] = 'A' + first;
        result_string[pointer + 1] = 'A' + second;
        result_string[pointer + 2] = '+';

        pointer += 3;

        vertices[first]++;
        vertices[second]++;
    }
}

void remove_edges(unsigned char first, unsigned char second, unsigned char n) {
    while (n--) {
        result_string[pointer] = 'A' + first;
        result_string[pointer + 1] = 'A' + second;
        result_string[pointer + 2] = '-';

        pointer += 3;

        vertices[first]--;
        vertices[second]--;
    }
}

// Обнуляет максимального соседа вершины с максимальным количеством дуонов
void set_max_neighbor_to_zero(unsigned char offset) {
    unsigned char max_vertex = max_element_pos(offset + 0, offset + 4);
    unsigned char neighbor = (vertices[offset + (max_vertex + 3) % 4] > vertices[offset + (max_vertex + 1) % 4]) ?
                             offset + (max_vertex + 3) % 4 :
                             offset + (max_vertex + 1) % 4;

    remove_edges(max_vertex, neighbor, vertices[neighbor]);
}

// Оставляет единственную вершину на грани
unsigned char leave_max(unsigned char offset) {
    unsigned char max_vertex = max_element_pos(offset, offset + 4);
    unsigned char almost_max_vertex = max_element_pos_crossed(offset, offset + 4, max_vertex);

    if (are_connected(max_vertex, almost_max_vertex)) {
        remove_edges(max_vertex, almost_max_vertex, vertices[almost_max_vertex]);
    } else {
        add_edges(offset + (almost_max_vertex + 1) % 4, max_vertex, vertices[almost_max_vertex]);
        remove_edges(offset + (almost_max_vertex + 1) % 4, almost_max_vertex, vertices[almost_max_vertex]);
    }

    return max_vertex;
}

unsigned char final_processing(unsigned char first_side_max, unsigned char second_side_max) {
    if (vertices[first_side_max] == 0 && vertices[second_side_max] == 0) {
        return 1;
    }

    if (vertices[first_side_max] != vertices[second_side_max] || are_connected(first_side_max + 4, second_side_max)) {
        return 0;
    }

    if (first_side_max + 4 == second_side_max) {
        remove_edges(first_side_max, second_side_max, vertices[first_side_max]);
        return 1;
    }

    add_edges((first_side_max + 3) % 4, same_side_diagonal(first_side_max), vertices[first_side_max]);
    remove_edges(second_side_max, same_side_diagonal(first_side_max), vertices[second_side_max]);
    remove_edges(first_side_max, (first_side_max + 3) % 4, vertices[first_side_max]);

    return 1;
}

int main() {
    for (unsigned char i = 0; i < 8; i++) {
        scanf("%hhu", &vertices[i]);
    }

    set_max_neighbor_to_zero(0);
    set_max_neighbor_to_zero(0);
    set_max_neighbor_to_zero(4);
    set_max_neighbor_to_zero(4);

    if (final_processing(leave_max(0), leave_max(4))) {
        for (unsigned short i = 0; i < pointer; i += 3) {
            printf("%c%c%c\n", result_string[i], result_string[i + 1], result_string[i + 2]);
        }
    } else {
        printf("%s\n", "IMPOSSIBLE");
    }

    return 0;
}
