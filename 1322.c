#include <stdio.h>
#include <string.h>

unsigned char get_priority(const char c) {
    return c >= 'A' && c <= 'Z' ?
           c - 'A' :
           c == '_' ? 26 : c - 'a' + 27;
}

int main() {
    char str[100001];

    unsigned int position[100000];

    unsigned int n;

    scanf("%u %s", &n, str);

    unsigned int index = n - 1;

    unsigned int str_length = strlen(str);

    int chars_number[54] = {0};

    for (unsigned int i = 0; i < str_length; i++) {
        chars_number[get_priority(str[i]) + 1]++;
    }

    for (unsigned char i = 2; i < 53; i++) {
        chars_number[i] += chars_number[i - 1];
    }

    for (unsigned int i = 0; i < str_length; i++) {
        unsigned char priority = get_priority(str[i]);
        position[chars_number[priority]] = i;
        chars_number[priority]++;
    }

    for (unsigned int i = 0; i < str_length; i++) {
        index = position[index];
        putchar(str[index]);
    }

    return 0;
}
