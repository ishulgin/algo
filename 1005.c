#define min(a, b) (((a) < (b)) ? (a) : (b))

#include <stdio.h>
#include <math.h>

unsigned short n;
unsigned int stones[20];
unsigned int minimum = 0xFFFFFFFF;

void process_sums(unsigned short i, unsigned int first_sum, unsigned int second_sum) {
	if (i == (n - 1)) {
		minimum = min(minimum, abs(first_sum - second_sum - stones[i]));
	} else {
		process_sums(i + 1, first_sum + stones[i], second_sum);
		process_sums(i + 1, first_sum, second_sum + stones[i]);
	}
}

int main() {
	scanf("%hu", &n);

	for (int i = 0; i < n; i++) {
		scanf("%u", &stones[i]);
	}

	if (n == 1) {
		printf("%u\n", stones[0]);
	} else {
		process_sums(0, 0, 0);
		printf("%u\n", minimum);
	}

	return 0;
}
