#define figure_number (figure_counter++ / 3)
#define new_hole_x (x + n / 2 - 1 + i)
#define new_hole_y (y + n / 2 - 1 + j)
#define left_x (x + i * n / 2)
#define right_x (x + i * n / 2 + n / 2)
#define top_y (y + j * n / 2)
#define bottom_y (y + j * n / 2 + n / 2)

#include <stdio.h>

//2^9 = 512))0
unsigned int figure_counter = 3, cells[512][512];

void process_board(unsigned short n, unsigned short x, unsigned short y, unsigned short hole_x, unsigned short hole_y) {
	// Минимальный размер поддоски - 2х2
	if (n == 2) {
		for (unsigned char i = 0; i < 2; i++)
			for (unsigned char j = 0; j < 2; j++)
				if (x + i != hole_x || y + j != hole_y)
					cells[x + i][y + j] = figure_number;
	}
	else {
		// Доска разбивается на 4 поддоски
		for (unsigned char i = 0; i < 2; i++)
			for (unsigned char j = 0; j < 2; j++)
				// Содержит ли поддоска дырку?
				if (left_x <= hole_x && right_x > hole_x &&
					top_y <= hole_y && bottom_y > hole_y)
					process_board(n / 2, left_x, top_y, hole_x, hole_y);
				else
					process_board(n / 2, left_x, top_y, new_hole_x, new_hole_y);
		// Обработка центральной фигуры (эта обработка должна происходить атомарно)
		for (unsigned char i = 0; i < 2; i++)
			for (unsigned char j = 0; j < 2; j++)
				if (left_x > hole_x || right_x <= hole_x ||
					top_y > hole_y || bottom_y <= hole_y)
					cells[new_hole_x][new_hole_y] = figure_number;
	}
}

int main() {
	unsigned char n;
	unsigned short x, y;
	scanf("%hhu %hu %hu", &n, &x, &y);
	unsigned short side_length = 1 << n;
	process_board(side_length, 0, 0, x - 1, y - 1);
	for (unsigned short i = 0; i < side_length; i++) {
		for (unsigned short j = 0; j < side_length; j++)
			printf("%u ", cells[i][j]);
		printf("\n");
	}
}
