#include <stdio.h>

int main() {
    short result[100] = {0, [1 ... 99] = -1};
    unsigned short map[100][100] = {0};
    unsigned short process[100], current_neighbor, current_country;
    unsigned short this_pointer, next_pointer;

    unsigned short n;
    scanf("%hu", &n);

    for (unsigned short i = 0; i < n; i++) {
        for (;;) {
            scanf("%hu", &current_neighbor);

            if (!current_neighbor) {
                break;
            }

            current_neighbor--;

            map[i][current_neighbor] = 1;
            map[current_neighbor][i] = 1;
        }
    }

    for (unsigned short i = 0; i < n; i++) {
        this_pointer = 0;
        next_pointer = 1;

        process[0] = i;

        while (this_pointer != next_pointer) {
            current_country = process[this_pointer];

            for (unsigned short j = 0; j < n; j++) {
                if (!map[current_country][j]) {
                    
                } else if (result[j] == -1) {
                    process[next_pointer] = j;

                    result[j] = 1 - result[current_country];

                    next_pointer++;
                } else if (result[j] == result[current_country]) {
                    goto is_impossible;
                }
            }

            this_pointer++;
        }
    }


    for (unsigned short i = 0; i < n; i++) {
        printf("%d", result[i]);
    }

    return 0;

    is_impossible:
    printf("-1");

    return 0;
}
