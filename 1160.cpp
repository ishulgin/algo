#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

struct node {
    node *root;

    unsigned int number;

    node(unsigned int n) :
            number(n),
            root(this) {};

    node *get_updated_root() {
        while (this->root != this->root->root) {
            this->root = this->root->root;
        }

        return this->root;
    };
};

struct edge {
    node *fst;
    node *snd;

    unsigned int weight;

    edge(node *fst, node *snd, unsigned int weight) :
            fst(fst),
            snd(snd),
            weight(weight) {};
};

int main() {
    vector<node> nodes;
    vector<edge> edges;
    vector<edge> tree_edges;

    unsigned int n, m;
    unsigned int fst, snd, weight;
    unsigned int max_distance = 0;

    scanf("%u %u", &n, &m);

    nodes.reserve(n);
    edges.reserve(m);
    tree_edges.reserve(m);

    for (unsigned int i = 0; i < n; i++) {
        nodes.emplace_back(i + 1);
    }

    for (unsigned int i = 0; i < m; i++) {
        scanf("%u %u %u", &fst, &snd, &weight);

        edges.emplace_back(&nodes[fst - 1], &nodes[snd - 1], weight);
    }

    sort(edges.begin(), edges.end(), [](edge a, edge b) { return a.weight < b.weight; });

    for (auto edge : edges) {
        if (edge.fst->get_updated_root() != edge.snd->get_updated_root()) {
            edge.fst->root->root = edge.snd->root->root;
            tree_edges.push_back(edge);

            max_distance = max(max_distance, edge.weight);
        }
    }

    printf("%u\n", max_distance);
    printf("%u\n", (unsigned int) tree_edges.size());

    for (auto edge : tree_edges) {
        printf("%d %d\n", edge.fst->number, edge.snd->number);
    }

    return 0;
}